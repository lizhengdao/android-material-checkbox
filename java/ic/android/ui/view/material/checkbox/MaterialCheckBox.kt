package ic.android.ui.view.material.checkbox


import ic.base.primitives.bool.to
import ic.base.primitives.float32.asInt32
import ic.base.primitives.int32.Int32

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import ic.android.ui.view.ext.setOnClickAction

import ic.android.util.units.dpToPx


class MaterialCheckBox : ImageView {


	init {
		scaleType = ScaleType.CENTER
	}


	override fun onMeasure (widthMeasureSpec: Int32, heightMeasureSpec: Int32) {
		setMeasuredDimension(
			dpToPx(48).asInt32,
			dpToPx(48).asInt32
		)
	}


	var isChecked : Boolean = false
		set (value) {
			field = value
			updateUi()
			onCheckedStateChangedAction(value, isOnCheckedStateChangedActionCalledByUserInteraction)
			isOnCheckedStateChangedActionCalledByUserInteraction = false
		}
	;


	private fun updateUi() {
		setImageResource(
			isChecked.to(
				R.drawable.icon_checkbox_checked_24dp,
				R.drawable.icon_checkbox_unchecked_24dp,
			)
		)
	}


	private var onCheckedStateChangedAction : (isChecked: Boolean, isCalledByUserInteraction: Boolean) -> Unit = { _, _ -> }

	private var isOnCheckedStateChangedActionCalledByUserInteraction : Boolean = false

	fun setOnCheckedStateChangedAction (
		toCallAtOnce : Boolean = false,
		onCheckedStateChangedAction : (isChecked: Boolean, isCalledByUserInteraction: Boolean) -> Unit
	) {
		this.onCheckedStateChangedAction = onCheckedStateChangedAction
		if (toCallAtOnce) {
			onCheckedStateChangedAction(isChecked, false)
		}
	}


	init {

		isClickable = true

		setOnClickAction {
			isOnCheckedStateChangedActionCalledByUserInteraction = true
			isChecked = !isChecked
		}

	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
		: super (context, attrs, defStyleAttr)
	;


}